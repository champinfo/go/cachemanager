package tests

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/champinfo/go/cachemanager"
	"testing"
)

func TestRedisMgr_SETEX(t *testing.T) {
	seconds := 200
	if s, err := cachemanager.Mgr.SetEx(SetExSecondsKey, seconds, Value1); err != nil {
		assert.Nil(t, err)
	} else {
		if s != "OK" {
			t.Error("setex failed")
		}
	}
}

func TestRedisMgr_ExpireDays(t *testing.T) {
	days := 1

	if _, err := cachemanager.Mgr.Set(ExpireDaysKey, Value1); err != nil {
		assert.Nil(t, err)
	}
	if i, err := cachemanager.Mgr.ExpireDays(ExpireDaysKey, days); err != nil {
		assert.Nil(t, err)
	} else {
		log.Debugln(i)
	}
	if i, err := cachemanager.Mgr.QueryTTL(ExpireDaysKey); err != nil {
		assert.Nil(t, err)
	} else {
		log.Debugln(i)
		if i != days*86400 {
			t.Error("ttl is not right")
		}
	}
}

func TestRedisMgr_ExpireSeconds(t *testing.T) {
	// create first
	if _, err := cachemanager.Mgr.Set(ExpireSecondsKey, Value1); err != nil {
		assert.Nil(t, err)
	}

	if i, err := cachemanager.Mgr.ExpireSeconds(ExpireSecondsKey, 10); err != nil {
		assert.Nil(t, err)
	} else {
		log.Debugln(i)
	}

	if i, err := cachemanager.Mgr.QueryTTL(ExpireSecondsKey); err != nil {
		assert.Nil(t, err)
	} else {
		if i != 10 {
			t.Error("ttl is not right")
		}
	}
}
