package tests

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/champinfo/go/cachemanager"
	"testing"
)

func TestRedisMgr_HashSet(t *testing.T) {
	if n, err := cachemanager.Mgr.HSet(HashKey, SubKey1, Value1); err != nil {
		t.Error(err.Error())
	} else {
		if n == 0 {
			log.Debugln(fmt.Sprintf("hash %s - %s - %s is already exist", HashKey, SubKey1, Value1))
		}
	}
	if n, err := cachemanager.Mgr.HSet(HashKey, SubKey2, Value2); err != nil {
		t.Error(err.Error())
	} else {
		if n == 0 {
			log.Debugln(fmt.Sprintf("hash %s - %s - %s is already exist", HashKey, SubKey2, Value2))
		}
	}
	if n, err := cachemanager.Mgr.HSet(HashKey, SubKey3, Value3); err != nil {
		t.Error(err.Error())
	} else {
		if n == 0 {
			log.Debugln(fmt.Sprintf("hash %s - %s - %s is already exist", HashKey, SubKey3, Value3))
		}
	}

	if ns, err := cachemanager.Mgr.HKeys(HashKey); err != nil {
		assert.Nil(t, err)
	} else {
		for _, n := range ns {
			log.Debugln(string(n))
		}
		if len(ns) != 3 {
			t.Errorf("# of subkeys should be %d, but get %d", 3, len(ns))
		}
	}

	if n, err := cachemanager.Mgr.HLen(HashKey); err != nil {
		assert.Nil(t, err)
	} else {
		log.Debugln(n)
		if n != 3 {
			t.Errorf("# of subkeys should be %d, but get %d", 3, n)
		}
	}
}

func TestRedisMgr_HashGet(t *testing.T) {
	if b, err := cachemanager.Mgr.HGet(HashKey, SubKey1); err != nil {
		t.Error(err.Error())
	} else {
		if string(b) != Value1 {
			t.Error(fmt.Sprintf("get the wrong value %v, expect %v", string(b), Value1))
		}
	}
}

func TestRedisMgr_HashDelete(t *testing.T) {
	if n, err := cachemanager.Mgr.HDel(HashKey, SubKey1); err != nil {
		t.Error(err.Error())
	} else {
		if n == 0 {
			log.Debugln(fmt.Sprintf("delete the duplicated subkey %v", SubKey1))
		}
	}
}

func TestRedisMgr_HashGetAll(t *testing.T) {
	if ss, err := cachemanager.Mgr.HGetAll(HashKey); err != nil {
		t.Error(err.Error())
	} else {
		for _, s := range ss {
			log.Debugln(s)
		}
	}
}
