package tests

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/champinfo/go/cachemanager"
	"log"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	SetupRedisConn()
	code := m.Run()
	TeardownRedisConn()
	os.Exit(code)
}

func TestNewRedisManager(t *testing.T) {
	/** arrange */
	redisConfig := cachemanager.Config{
		Mode:               1,
		Password:           "champ@redis",
		Addr:               []string{"localhost:26379", "localhost:26380", "localhost:26381"},
		MasterName:         "mymaster",
		Db:                 0,
		Wait:               false,
		MaxIdleConnections: 80,
		MaxActive:          0,
		IdleTimeout:        240,
	}
	/** act */
	err := cachemanager.Mgr.Init(&redisConfig)

	/** assert */
	assert.Nil(t, err)

	s, err := cachemanager.Mgr.Ping()
	if err != nil {
		t.Error(err.Error())
	}
	if s != "PONG" {
		t.Error(errors.New("can not get pong"))
	}
	log.Println(s)
}
