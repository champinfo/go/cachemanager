package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/champinfo/go/cachemanager"
	"strconv"
	"testing"
)

func TestRedisMgr_IncreaseBy(t *testing.T) {
	//create key-value first
	_, err := cachemanager.Mgr.Set(IncreaseByKey, IncreaseByValue)
	assert.Nil(t, err)

	_, err = cachemanager.Mgr.INCRBY(IncreaseByKey, IncreaseByValue+2)
	assert.Nil(t, err)

	if v, err := cachemanager.Mgr.Get(IncreaseByKey); err != nil {
		t.Error(err.Error())
	} else {
		i, _ := strconv.Atoi(v)
		if i != IncreaseByValue+IncreaseByValue+2 {
			t.Error("wrong value")
		}
	}
}

func TestRedisMgr_DecreaseBy(t *testing.T) {
	times := 2
	_, err := cachemanager.Mgr.Set(DecreaseByKey, DecreaseByValue)
	assert.Nil(t, err)

	_, err = cachemanager.Mgr.DECRBY(DecreaseByKey, times)
	assert.Nil(t, err)

	if v, err := cachemanager.Mgr.Get(DecreaseByKey); err != nil {
		t.Error(err.Error())
	} else {
		i, _ := strconv.Atoi(v)
		if i != DecreaseByValue-times {
			t.Error("wrong value")
		}
	}
}
