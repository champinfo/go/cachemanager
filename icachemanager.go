package cachemanager

type ICacheManager interface {
	Init(config *Config) error
	Close()

	Ping() (string, error)

	ExpireSeconds(key string, seconds int) (int64, error)
	ExpireDays(key string, days int) (int64, error)
	QueryTTL(key string) (int, error)

	HSet(key, subKey string, value interface{}) (int64, error)
	HGet(key, subKey string) ([]byte, error)
	HDel(key, subKey string) (int, error)
	HKeys(key string) ([][]byte, error)
	HLen(key string) (int, error)
	DeleteAll() (string, error)
	DeleteDb() ([]string, error)
	HGetAll(key string) ([]string, error)
	SetEx(key string, seconds int, value interface{}) (string, error)
	Set(key string, value interface{}) (string, error)
	Get(key string) (string, error)
	Delete(key string) (int, error)
	INCRBY(key string, times int) (int, error)
	DECRBY(key string, times int) (int, error)
	HINCRBY(key, subKey string, times int) (int, error)
}

type Config struct {
	Mode               int    `json:"Mode" yaml:"Mode"`
	Password           string `required:"true" json:"Password" yaml:"Password"`
	Addr               string `required:"true" json:"Addr" yaml:"Addr"`
	MasterName         string `json:"MasterName" yaml:"MasterName"`
	Db                 int    `default:"0" json:"Db" yaml:"Db"`
	Wait               bool   `json:"Wait" yaml:"Wait"`
	MaxIdleConnections int    `default:"1024" json:"MaxIdleConnections" yaml:"MaxIdleConnections"`
	MaxActive          int    `default:"0" json:"MaxActive" yaml:"MaxActive"`
	IdleTimeout        int    `default:"240" json:"IdleTimeout" yaml:"IdleTimeout"`
}
