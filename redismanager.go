package cachemanager

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/letsfire/redigo/v2"
	"github.com/letsfire/redigo/v2/mode/alone"
	"github.com/letsfire/redigo/v2/mode/cluster"
	"github.com/letsfire/redigo/v2/mode/sentinel"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"
)

const (
	Select       = "SELECT"
	Ping         = "Ping"
	HashSet      = "HSET"
	HashGet      = "HGET"
	HashGetAll   = "HGETALL"
	HashDelete   = "HDEL"
	HashKeys     = "HKEYS"
	HashLen      = "HLen"
	Expire       = "EXPIRE"
	TTL          = "TTL"
	Keys         = "KEYS"
	FlushAllKeys = "FLUSHALL"
	StringSet    = "SET"
	StringGet    = "GET"
	StringDelete = "DEL"

	SetKeyValueAndExpire = "SETEX"

	HashIncreaseBy = "HINCRBY"
	IncreaseBy     = "INCRBY"
	DecreaseBy     = "DECRBY"
)

var Mgr ICacheManager

func init() {
	Mgr = &redismanager{}
}

func release(conn redis.Conn) {
	if err := conn.Close(); err != nil {
		log.Errorln("close connection error ", err.Error())
	}
}

type redismanager struct {
	client *redigo.Client
}

func newAlone(config *Config) (*redigo.Client, error) {
	var aloneMode = alone.New(
		alone.Addr(config.Addr),
		alone.PoolOpts(
			redigo.MaxActive(config.MaxActive),
			redigo.MaxIdle(config.MaxIdleConnections),
			redigo.Wait(config.Wait),
			redigo.IdleTimeout(time.Duration(config.IdleTimeout)*time.Second),
			redigo.MaxConnLifetime(0),
			redigo.TestOnBorrow(func(c redis.Conn, t time.Time) (err error) {
				_, err = c.Do(Ping)
				return err
			}),
		),
		alone.DialOpts(
			redis.DialReadTimeout(time.Second),
			redis.DialWriteTimeout(time.Second),
			redis.DialConnectTimeout(time.Second),
			redis.DialPassword(config.Password),
			redis.DialDatabase(config.Db),
			redis.DialKeepAlive(time.Minute*5),
			redis.DialUseTLS(false),
			redis.DialTLSSkipVerify(false),
		),
	)

	client := redigo.New(aloneMode)
	return client, nil
}

func newSentinel(config *Config) (*redigo.Client, error) {
	addrs := strings.Split(config.Addr, ";")
	var sentinelMode = sentinel.New(
		sentinel.Addrs(addrs),
		sentinel.MasterName(config.MasterName),
		sentinel.PoolOpts(
			redigo.MaxActive(config.MaxActive),
			redigo.MaxIdle(config.MaxIdleConnections),
			redigo.Wait(config.Wait),
			redigo.IdleTimeout(time.Duration(config.IdleTimeout)*time.Second),
			redigo.MaxConnLifetime(0),
			redigo.TestOnBorrow(func(c redis.Conn, t time.Time) (err error) {
				_, err = c.Do(Ping)
				return err
			})),
		sentinel.DialOpts(
			redis.DialReadTimeout(time.Second),
			redis.DialWriteTimeout(time.Second),
			redis.DialConnectTimeout(time.Second),
			redis.DialPassword(config.Password),
			redis.DialDatabase(config.Db),
			redis.DialKeepAlive(time.Minute*5),
			redis.DialUseTLS(false),
			redis.DialTLSSkipVerify(false),
		))
	client := redigo.New(sentinelMode)
	return client, nil
}

func newCluster(config *Config) (*redigo.Client, error) {
	nodes := strings.Split(config.Addr, ";")
	var clusterMode = cluster.New(
		cluster.Nodes(nodes),
		cluster.PoolOpts(
			redigo.MaxActive(config.MaxActive),
			redigo.MaxIdle(config.MaxIdleConnections),
			redigo.Wait(config.Wait),
			redigo.IdleTimeout(time.Duration(config.IdleTimeout)*time.Second),
			redigo.MaxConnLifetime(0),
			redigo.TestOnBorrow(func(c redis.Conn, t time.Time) (err error) {
				_, err = c.Do(Ping)
				return err
			})),
		cluster.DialOpts(
			redis.DialReadTimeout(time.Second),
			redis.DialWriteTimeout(time.Second),
			redis.DialConnectTimeout(time.Second),
			redis.DialPassword(config.Password),
			redis.DialDatabase(config.Db),
			redis.DialKeepAlive(time.Minute*5),
			redis.DialUseTLS(false),
			redis.DialTLSSkipVerify(false),
		))
	client := redigo.New(clusterMode)
	return client, nil
}

//Init 初始化 Redis pool
func (rm *redismanager) Init(config *Config) error {
	var client *redigo.Client
	var err error

	switch config.Mode {
	case 0:
		client, err = newAlone(config)
	case 1:
		client, err = newSentinel(config)
	case 2:
		client, err = newCluster(config)
	default:
		client, err = newAlone(config)
	}
	if err != nil {
		log.Println("initialize redis pool fail, error ", err.Error())
		return err
	}
	rm.client = client
	ms := rm.client.Mode()
	log.Debugln("current redis client mode is ", ms)
	_, err = rm.Ping()
	if err != nil {
		log.Errorln("ping redis server error ", err.Error())
		return err
	}
	return nil
}

//Close 關閉 Redis pool
func (rm *redismanager) Close() {
	err := rm.client.Close()
	if err != nil {
		log.Errorln(err.Error())
	}
}

//Ping 測試 Redis pool 服務狀況
func (rm *redismanager) Ping() (string, error) {
	s, err := rm.client.Bytes(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(Ping)
	})
	return string(s), err
}

//HSet Stores the value at the key in the hash
func (rm *redismanager) HSet(key, subKey string, value interface{}) (int64, error) {
	i, err := rm.client.Int64(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashSet, key, subKey, value)
	})
	checkError(key, err)
	return i, err
}

//HGet Fetches the value at the given hash key
func (rm *redismanager) HGet(key, subKey string) ([]byte, error) {
	b, err := rm.client.Bytes(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashGet, key, subKey)
	})
	checkError(key, err)
	return b, err
}

//HDel Removes a key from the hash, if it exists
func (rm *redismanager) HDel(key, subKey string) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashDelete, key, subKey)
	})
	checkError(key, err)
	return i, err
}

//HGetAll Fetches the entire hash
func (rm *redismanager) HGetAll(key string) ([]string, error) {
	ss, err := rm.client.Strings(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashGetAll, key)
	})
	checkError(key, err)
	return ss, err
}

//HKeys 顯示目前Hash內key裡面的SubKeys
func (rm *redismanager) HKeys(key string) ([][]byte, error) {
	bs, err := rm.client.Values(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashKeys, key)
	})
	ds := make([][]byte, 0)
	for _, value := range bs {
		ds = append(ds, value.([]byte))
		log.Debugf("%s ", string(value.([]byte)))
	}
	checkError(key, err)
	return ds, err
}

//HLen 顯示目前Hash內key裡面的SubKeys長度
func (rm *redismanager) HLen(key string) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashLen, key)
	})
	checkError(key, err)
	return i, err
}

//ExpireSeconds 設置過期秒數
func (rm *redismanager) ExpireSeconds(key string, seconds int) (int64, error) {
	i, err := rm.client.Int64(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(Expire, key, seconds)
	})
	checkError(key, err)
	return i, err
}

//ExpireDays 設置過期天數
func (rm *redismanager) ExpireDays(key string, days int) (int64, error) {
	i, err := rm.client.Int64(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(Expire, key, days*86400)
	})
	checkError(key, err)
	return i, err
}

//QueryTTL 查詢過期時間
func (rm *redismanager) QueryTTL(key string) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(TTL, key)
	})
	checkError(key, err)
	return i, err
}

//Delete the value stored at the given key
func (rm *redismanager) Delete(key string) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(StringDelete, key)
	})
	checkError(key, err)
	return i, err
}

//DeleteAll 刪除所有資料
func (rm *redismanager) DeleteAll() (string, error) {
	s, err := rm.client.String(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(FlushAllKeys)
	})
	return s, err
}

//DeleteDb 刪除當前使用database，而不是全刪除
func (rm *redismanager) DeleteDb() ([]string, error) {
	ss, err := rm.client.Strings(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(Keys, "*")
	})
	return ss, err
}

//SetEx 設定key value and expire seconds
func (rm *redismanager) SetEx(key string, seconds int, value interface{}) (string, error) {
	s, err := rm.client.String(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(SetKeyValueAndExpire, key, seconds, value)
	})
	return s, err
}

//Set the value stored at the given key
func (rm *redismanager) Set(key string, value interface{}) (string, error) {
	s, err := rm.client.String(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(StringSet, key, value)
	})
	checkError(key, err)
	return s, err
}

//Get Fetches the data stored at the given key
func (rm *redismanager) Get(key string) (string, error) {
	s, err := rm.client.String(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(StringGet, key)
	})
	checkError(key, err)
	return s, err
}

//INCRBY 累加key次數
func (rm *redismanager) INCRBY(key string, times int) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(IncreaseBy, key, times)
	})
	checkError(key, err)
	return i, err
}

func (rm *redismanager) DECRBY(key string, times int) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(DecreaseBy, key, times)
	})
	checkError(key, err)
	return i, err
}

//HINCRBY increase a value in a hash by key
func (rm *redismanager) HINCRBY(key string, subKey string, times int) (int, error) {
	i, err := rm.client.Int(func(c redis.Conn) (res interface{}, err error) {
		return c.Do(HashIncreaseBy, key, subKey, times)
	})
	checkError(key, err)
	return i, err
}

func checkError(key string, err error) {
	if err != nil {
		log.Println(fmt.Errorf("error get key %s: %v", key, err))
	}
}
