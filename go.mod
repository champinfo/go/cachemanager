module gitlab.com/champinfo/go/cachemanager

go 1.17

require (
	github.com/gomodule/redigo v1.8.8
	github.com/letsfire/redigo/v2 v2.0.5
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/FZambia/sentinel v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mna/redisc v1.3.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
